DEPENDS += "opensc-native libp11-native p11-kit-native python3-pkcs11-sign-native"

export OPENSSL_ENGINES = "${STAGING_DIR_NATIVE}/${prefix_native}/lib/engines-3"
export OPENSSL_MODULES = "${STAGING_DIR_NATIVE}/${prefix_native}/lib/ossl-modules"
export PKCS11_MODULE_PATH = "${STAGING_DIR_NATIVE}/${prefix_native}/lib/p11-kit-proxy.so"
