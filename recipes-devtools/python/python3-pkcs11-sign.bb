LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DESCRIPTION = "Support for PKCS#11 signing"

inherit python3-dir setuptools3-base

SRC_URI = " \
	file://pkcs11_rsa.py \
"

do_install:append () {
  install -d ${D}${PYTHON_SITEPACKAGES_DIR}
  install -m 0644 ${WORKDIR}/pkcs11_rsa.py ${D}${PYTHON_SITEPACKAGES_DIR}/
}

RDEPENDS:${PN} = "${PYTHON_PN}-cryptography"

BBCLASSEXTEND = "native"
