from cryptography.hazmat.backends.openssl import rsa
import cryptography.hazmat.backends.openssl.backend as openssl_backend


class PKCS11RSAPrivateKey(rsa._RSAPrivateKey):

    def __init__(self, key_id):
        pkcs11_eng = openssl_backend._lib.ENGINE_by_id(b"pkcs11")
        if pkcs11_eng == openssl_backend._ffi.NULL:
            raise RuntimeError("Failed to load PKCS11 backend!")

        res = openssl_backend._lib.ENGINE_init(pkcs11_eng)
        if res != 1:
            raise RuntimeError("Failed to initialize PKCS11 engine")

        pkey = openssl_backend._lib.ENGINE_load_private_key(
            pkcs11_eng,
            key_id.encode(),
            openssl_backend._ffi.NULL,
            openssl_backend._ffi.NULL,
        )

        if pkey == openssl_backend._ffi.NULL:
            raise RuntimeError(f"Failed to load PKCS11 private key '{key_id}'!")

        rsa = openssl_backend._lib.EVP_PKEY_get1_RSA(pkey)
        if rsa == openssl_backend._ffi.NULL:
            raise RuntimeError("Failed to get PKCS11 RSA private key")

        super().__init__(
            backend=openssl_backend,
            rsa_cdata=rsa,
            evp_pkey=pkey,
            _skip_check_key=True,
        )
