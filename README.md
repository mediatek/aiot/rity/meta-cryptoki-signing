# meta-cryptoki-signing

This layer is the collection of recipes used for running PKCS#11 signing.
The layer contains the following:

* PKCS#11 python tool for RSA signing

## Dependencies

This layer depends on following layers:

* core
* [meta-mediatek-bsp](https://gitlab.com/mediatek/aiot/rity/meta-mediatek-bsp)

## How to use

Suppose that we have a RITY building environment, clone the repository under
`src` folder:

    git clone https://gitlab.com/mediatek/aiot/rity/meta-cryptoki-signing <RITY_PROJECT>/src/meta-cryptoki-signing

Run following command to add this layer to BitBake building environment

    bitbake-layers add-layer ../src/meta-cryptoki-signing

And edit `local.conf` to add following setting to select PKCS11 URI and which module to be signed:

    PKCS11_URI = "<pkcs11-uri>"

    SECURE_BOOT_ROT_KEY = "${PKCS11_URI}"
    DA_KEY_PKCS11_URI = "${SECURE_BOOT_ROT_KEY}"
    EFUSE_KEY_PKCS11_URI = "${SECURE_BOOT_ROT_KEY}"

    DISTRO_FEATURES:append = " cryptoki-sign"
    DISTRO_FEATURES:append = " secure-boot"
    BL2_SIGN_ENABLE = "1"
    DA_SIGN_ENABLE = "1"
    SECURE_ZIP_ENABLE = "1"

For more information on how to construct a proper `PKCS11_URI`, please see the U-Boot documentation:

* [FIT image signatures](https://github.com/u-boot/u-boot/blob/master/doc/usage/fit/signature.rst?plain=1#L500)

This would use the same key for all signatures, though in principle it should be possible to specify different
keys for the different use cases.

And follow the usual way to build image:

    bitbake rity-demo-image

To disable PKCS#11 signing, remove the layer and edit `local.conf` to remove the DISTRO_FEATURES `cryptoki-sign`:

    bitbake-layers remove-layer ../src/meta-cryptoki-signing

And rebuild the image:

    bitbake rity-demo-image
